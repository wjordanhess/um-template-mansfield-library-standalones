<? include('/var/www/drupal/local/stand_alones/um_std_tpl/template-header-tpl.html'); ?>
Page Breadcrumb Title
<? // include('/var/www/drupal/local/stand_alones/um_std_tpl/template-nav-tpl.html'); ?>
<? // Include either NAV or NONAV ?>
<? include('/var/www/drupal/local/stand_alones/um_std_tpl/template-no-nav-tpl.html'); ?>
<? // Begin Page Content ?>
<h1>Page Title Here</h1>
<div class="row-fluid row-simple">
	<div class="span12">
		<h2>Example of Single Column</h2>
		<p>Maybe we better talk out here; the observation lounge has turned into a swamp.</p>
	</div>
</div>
<div class="row-fluid row-simple">
	<div class="span6">
		<h2>Example of Two Columns</h2>
		<p>Fate. It protects fools, little children, and ships named "Enterprise."</p>
	</div>
	<div class="span6">
		<h2>Another Column</h2>
		<p>And if I had it to do over again, I would have grabbed the phaser and pointed it at you instead of them.</p>
	</div>
</div>
<? // End Page Content ?>
<? include('/var/www/drupal/local/stand_alones/um_std_tpl/template-footer-tpl.html'); ?>